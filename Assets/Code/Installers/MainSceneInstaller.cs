using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using Zenject;
using Zenject.SpaceFighter;

namespace ArcheroPrototype
{
    public class MainSceneInstaller : MonoInstaller
    {
        [SerializeField] private PlayerInput playerInput;
        [SerializeField] private Character player;
        [SerializeField] private GameObject enemyPrefab;
        [SerializeField] private Transform navMeshAgentsRoot;
        [SerializeField] private Transform charactersRoot;
        [SerializeField] private Transform bulletsRoot;

        public override void InstallBindings()
        {
            Container.Bind<PlayerInput>().FromInstance(playerInput).AsSingle();
            Container.Bind<CharacterModel>().FromNew().AsTransient();

            var enemyConstructArgs = new EnemyAI.ConstructArgs()
            {
                Target = player,
                NavMeshAgentsRoot = navMeshAgentsRoot
            };
            Container.Bind<EnemyAI.ConstructArgs>().FromInstance(enemyConstructArgs).AsTransient();

            var enemySpawnerConstructArgs = new EnemySpawner.ConstructArgs()
            {
                EnemyPrefab = enemyPrefab
            };
            Container.Bind<EnemySpawner.ConstructArgs>().FromInstance(enemySpawnerConstructArgs).AsTransient();

            var playerTargetsProviderArgs = new CharacterTargetsProvider.ConstructArgs()
            {
                CharactersRoot = charactersRoot
            };
            Container.Bind<CharacterTargetsProvider.ConstructArgs>().FromInstance(playerTargetsProviderArgs).AsTransient();

            var characterShooterArgs = new CharacterShooter.ConstructArgs()
            {
                parentForBullets = bulletsRoot
            };
            Container.Bind<CharacterShooter.ConstructArgs>().FromInstance(characterShooterArgs).AsTransient();
        }
    }
}