using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace ArcheroPrototype
{
    [RequireComponent(typeof(EnemyMovementController), typeof(CharacterRotator))]
    public class EnemyAI : MonoBehaviour
    {
        [SerializeField] private float randomPositionOffset;
        [SerializeField] private float timeForStay;
        [SerializeField] private float minimalDistanceToTarget;

        private Character target;
        private Transform anotherAgentRoot;

        private CharacterRotator characterRotator;
        private EnemyMovementController movementController;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            target = args.Target;
            anotherAgentRoot = args.NavMeshAgentsRoot;
        }

        private void Awake()
        {
            movementController = GetComponent<EnemyMovementController>();
            characterRotator = GetComponent<CharacterRotator>();
            movementController.Init();

            movementController.IsMoving.Subscribe(EnemyMovementController_IsMoving_OnPropertyChanged);
        }

        private void Update()
        {
            if (target == null)
            {
                return;
            }

            if (!movementController.IsMoving.Value)
            {
                characterRotator.SetLookRotation(target.transform.position - transform.position);
            }
        }

        private void EnemyMovementController_IsMoving_OnPropertyChanged(bool isMoving)
        {
            if (isMoving)
            {
                return;
            }

            if (CheckPointForShootPosibility())
            {
                StartCoroutine(StayProcess());
            }
            else
            {
                Vector3 pointNearTarget = GetPointNearTarget();
                movementController.DestinationPoint = pointNearTarget;
            }
        }

        private bool CheckPointForShootPosibility()
        {
            var rayToPlayerDirection = target.transform.position - transform.position;
            var rayToPlayer = new Ray(transform.position, rayToPlayerDirection);
            if (Physics.Raycast(rayToPlayer, out var hit, 30))
            {
                if (hit.collider.gameObject.tag == "Player")
                {
                    return true;
                }
            }

            return false;
        }

        private Vector3 GetPointNearTarget()
        {
            Vector3 pointNearTarget;

            int breaker = 0;
            
            do
            {
                breaker++;
                if (breaker > 100)
                {
                    Debug.LogError("Too many tries for find point near target with setted minimal distance", this);
                    return target.transform.position;
                }

                float xRandomOffset = Random.Range(-randomPositionOffset, randomPositionOffset);
                float zRandomOffset = Random.Range(-randomPositionOffset, randomPositionOffset);
                pointNearTarget = target.transform.position + new Vector3(xRandomOffset, 0, zRandomOffset);

                float toTargetDistanceSqr = Vector3.SqrMagnitude(pointNearTarget - target.transform.position);
                float minimalDistanceSqr = minimalDistanceToTarget * minimalDistanceToTarget;
                if (toTargetDistanceSqr < minimalDistanceSqr)
                {
                    continue;
                }

                if (!CheckForAnotherEnemiesPathCollision(pointNearTarget))
                {
                    continue;
                }

                return pointNearTarget;
            }
            while (true);

        }

        private IEnumerator StayProcess()
        {
            float timer = 0;
            do
            {
                timer += Time.deltaTime;
                if (timer > timeForStay)
                {
                    break;
                }

                yield return null;
            }
            while (CheckPointForShootPosibility());

            Vector3 pointNearTarget = GetPointNearTarget();
            movementController.DestinationPoint = pointNearTarget;
        }

        private bool CheckForAnotherEnemiesPathCollision(Vector3 destinationPoint)
        {
            foreach (var agent in anotherAgentRoot.GetComponentsInChildren<NavMeshAgent>())
            {
                if (agent == this)
                {
                    continue;
                }

                if (Vector3.SqrMagnitude(agent.destination - destinationPoint) < 2F)
                {
                    return false;
                }
            }

            return true;
        }

        public struct ConstructArgs
        {
            public Character Target;
            public Transform NavMeshAgentsRoot;
        }
    }
}