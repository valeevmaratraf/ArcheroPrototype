using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace ArcheroPrototype
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private Transform minSpawnAreaEdge;
        [SerializeField] private Transform maxSpawnAreaEdge;
        [SerializeField] private int enemyCount;
        [SerializeField] private Transform parent;

        private GameObject enemyPrefab;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            enemyPrefab = args.EnemyPrefab;
        }

        private void Start()
        {
            for (int i = 0; i < enemyCount; i++)
            {
                SpawnEnemy(i);
            }
        }

        private void SpawnEnemy(int index)
        {
            var enemyGO = Instantiate(enemyPrefab, parent);
            enemyGO.name = $"Enemy_{index}";
            enemyGO.transform.position = GetRandomSpawnPosition();
            enemyGO.GetComponent<NavMeshAgent>().avoidancePriority = index;
        }

        private Vector3 GetRandomSpawnPosition()
        {
            return new Vector3()
            {
                x = Random.Range(minSpawnAreaEdge.position.x, maxSpawnAreaEdge.position.x),
                y = Random.Range(minSpawnAreaEdge.position.y, maxSpawnAreaEdge.position.y),
                z = Random.Range(minSpawnAreaEdge.position.z, maxSpawnAreaEdge.position.z)
            };
        }

        public class ConstructArgs
        {
            public GameObject EnemyPrefab;
        }
    }
}
