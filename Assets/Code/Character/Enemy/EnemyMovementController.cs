using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.AI;

namespace ArcheroPrototype
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class EnemyMovementController : MonoBehaviour, IMovementController
    {
        private NavMeshAgent navMeshAgent;

        private ReactiveProperty<bool> isMoving;

        //private Coroutine unstucker;

        public ReadOnlyReactiveProperty<bool> IsMoving => isMoving.ToReadOnlyReactiveProperty();
        public Vector3 DestinationPoint
        {
            get => navMeshAgent.destination;
            set => navMeshAgent.destination = value;
        }
        public Vector3 MoveDirection => navMeshAgent.velocity;


        public void Init()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            isMoving = new ReactiveProperty<bool>();

            //isMoving.Subscribe((isMoving) =>
            //{
            //    if (isMoving)
            //    {
            //        unstucker = StartCoroutine(Unstucker());
            //    }
            //    if (!isMoving && unstucker != null)
            //    {
            //        StopCoroutine(unstucker);
            //    }
            //});
        }

        private void Update()
        {
            isMoving.Value = navMeshAgent.velocity != Vector3.zero;
        }

        //private IEnumerator Unstucker()
        //{
        //    Vector3 destination = navMeshAgent.destination;
        //    Vector3 origin = transform.position;

        //    while (true)
        //    {
        //        if (!isMoving.Value)
        //        {
        //            break;
        //        }

        //        if (Vector3.SqrMagnitude(origin - transform.position) < 1)
        //        {
        //            Debug.Log("reset");
        //            navMeshAgent.ResetPath();
        //            navMeshAgent.SetDestination(destination);
        //            origin = transform.position;
        //        }

        //        yield return new WaitForSeconds(2);
        //    }
        //}
    }
}
