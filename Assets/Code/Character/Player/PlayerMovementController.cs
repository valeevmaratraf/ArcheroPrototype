using System;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;
using Zenject;

namespace ArcheroPrototype
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerMovementController : MonoBehaviour, IMovementController
    {
        [SerializeField] private float speed;

        private CharacterController controller;

        private Vector2 movementInputDelta;

        private ReactiveProperty<bool> isMoving;

        public ReadOnlyReactiveProperty<bool> IsMoving => isMoving.ToReadOnlyReactiveProperty();
        public Vector3 MoveDirection => controller.velocity;

        [Inject]
        private void Construct(PlayerInput playerInput)
        {
            isMoving = new ReactiveProperty<bool>();
            playerInput.onActionTriggered += PlayerInput_onActionTriggered;
        }

        private void Awake()
        {
            controller = GetComponent<CharacterController>();
        }

        private void PlayerInput_onActionTriggered(InputAction.CallbackContext ctx)
        {
            if (ctx.action.name != "Move")
            {
                return;
            }

            movementInputDelta = ctx.ReadValue<Vector2>();
        }

        private void Update()
        {
            isMoving.Value = movementInputDelta != Vector2.zero;

            if (movementInputDelta == Vector2.zero)
            {
                return;
            }

            Vector3 moveDelta = new Vector3(movementInputDelta.x, 0, movementInputDelta.y);
            moveDelta *= Time.deltaTime * speed;
            controller.Move(moveDelta);
        }
    }
}