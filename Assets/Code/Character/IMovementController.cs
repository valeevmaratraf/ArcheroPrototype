using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace ArcheroPrototype
{
    public interface IMovementController
    {
        public ReadOnlyReactiveProperty<bool> IsMoving { get; }
        public Vector3 MoveDirection { get; }
    }
}
