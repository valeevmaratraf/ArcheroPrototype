using UniRx;
using UnityEngine;
using Zenject;

namespace ArcheroPrototype
{
    public enum Team { Player, Enemies }

    [RequireComponent(typeof(CharacterRotator))]
    public class Character : MonoBehaviour
    {
        [SerializeField] private Team team;

        private IMovementController movementController;
        private CharacterRotator rotator;
        private ITargetProvider targetProvider;

        //TODO: replace to model
        public Team Team => team;
        public CharacterModel CharacterModel { get; private set; }

        [Inject]
        private void Construct(CharacterModel model)
        {
            CharacterModel = model;
        }

        private void Awake()
        {
            targetProvider = GetComponent<ITargetProvider>();
            UnityEngine.Assertions.Assert.IsNotNull(targetProvider, gameObject.name);

            rotator = GetComponent<CharacterRotator>();
            movementController = GetComponent<IMovementController>();
            UnityEngine.Assertions.Assert.IsNotNull(movementController, gameObject.name);
        }

        private void Start()
        {
            movementController.IsMoving.Subscribe(MovementController_IsMoving_OnPropertyChanged);
        }

        private void Update()
        {
            //TODO: invert controls?
            if (movementController.IsMoving.Value && movementController.MoveDirection != Vector3.zero)
            {
                rotator.SetLookRotation(movementController.MoveDirection);
            }
            else
            {
                var target = targetProvider.GetTarget();
                rotator.SetLookRotation(target.transform.position - transform.position);
            }
        }

        private void MovementController_IsMoving_OnPropertyChanged(bool value)
        {
            CharacterModel.State = value ? CharacterState.Move : CharacterState.Idle;
        }
    }
}
