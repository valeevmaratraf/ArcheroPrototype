﻿namespace ArcheroPrototype
{
    public interface ITargetProvider
    {
        Character GetTarget();
    }
}
