using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcheroPrototype
{
    public class Bullet : MonoBehaviour
    {
        //TODO: add model?
        [SerializeField] private float speed;
        [SerializeField] private float damage;

        private void Update()
        {
            transform.position += transform.forward * Time.deltaTime * speed;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Destroy(gameObject);
        }
    }
}
