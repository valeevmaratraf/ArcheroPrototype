using ModestTree;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

namespace ArcheroPrototype
{
    [RequireComponent(typeof(Character))]
    public class CharacterShooter : MonoBehaviour
    {
        [SerializeField] private float reloadTime;
        [SerializeField] private Transform bulletSpawnPoint;
        [SerializeField] private GameObject bulletPrefab;

        private Character character;
        private ITargetProvider targetsProvider;
        private Transform bulletsParent;

        private Coroutine shootProcess;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            bulletsParent = args.parentForBullets;
        }

        private void Awake()
        {
            targetsProvider = GetComponent<ITargetProvider>();
            UnityEngine.Assertions.Assert.IsNotNull(targetsProvider);
            character = GetComponent<Character>();
        }

        private void Start()
        {
            character.CharacterModel.StateObservable.Subscribe(CharacterModelState_State_OnPropertyChanged);
        }

        private void CharacterModelState_State_OnPropertyChanged(CharacterState state)
        {
            if (state == CharacterState.Idle)
            {
                shootProcess = StartCoroutine(ShootProcess());
            }
            else if (state == CharacterState.Move)
            {
                if (shootProcess != null)
                {
                    StopCoroutine(shootProcess);
                    shootProcess = null;
                    return;
                }
            }
        }

        private IEnumerator ShootProcess()
        {
            while (true)
            {
                if (targetsProvider.GetTarget() != null)
                {
                    var bulletGO = Instantiate(bulletPrefab, bulletsParent);
                    bulletGO.transform.position = bulletSpawnPoint.position;
                    bulletGO.transform.rotation = bulletSpawnPoint.rotation;
                    bulletGO.name = $"{gameObject.name}Bullet";
                    yield return new WaitForSeconds(reloadTime);
                }
                else
                {
                    yield return null;
                }
            }
        }

        public class ConstructArgs
        {
            public Transform parentForBullets;
        }
    }
}
