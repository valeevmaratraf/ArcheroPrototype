using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace ArcheroPrototype
{
    [RequireComponent(typeof(Character))]
    public class CharacterTargetsProvider : MonoBehaviour, ITargetProvider
    {
        private Transform charactersRoot;
        private Character self;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            charactersRoot = args.CharactersRoot;
        }

        private void Awake()
        {
            self = GetComponent<Character>();
        }

        public Character GetTarget()
        {
            var characters = charactersRoot.GetComponentsInChildren<Character>();
            
            (Character, float)[] distanceToCharacterArr = new (Character, float)[characters.Length];
            float smallerDistance = float.MaxValue;

            for (int i = 0; i < characters.Length; i++)
            {
                Character character = characters[i];
                if (character.Team == self.Team)
                {
                    continue;
                }

                float distance = Vector3.SqrMagnitude(character.transform.position - transform.position);
                if (distance < smallerDistance)
                {
                    smallerDistance = distance;
                }

                distanceToCharacterArr[i] = new(character, distance);
            }

            foreach (var distanceCharacterPair in distanceToCharacterArr)
            {
                if (distanceCharacterPair.Item2 == smallerDistance)
                {
                    return distanceCharacterPair.Item1;
                }
            }

            return null;
        }


        public class ConstructArgs
        {
            public Transform CharactersRoot;
        }
    }
}
