using System.Collections;
using UnityEngine;

namespace ArcheroPrototype
{
    public class CharacterRotator : MonoBehaviour
    {
        [SerializeField] private float timeToRotate;

        private Coroutine currentRotationProcess;

        public void SetLookRotation(Vector3 lookDirection)
        {
            if (currentRotationProcess != null)
            {
                StopCoroutine(currentRotationProcess);
            }

            currentRotationProcess = StartCoroutine(RotationProcess(lookDirection));
        }

        private IEnumerator RotationProcess(Vector3 lookDirection)
        {
            var origin = transform.rotation;
            var destination = Quaternion.LookRotation(lookDirection, transform.up);
            float timer = 0;

            while (transform.rotation != destination)
            {
                timer += Time.deltaTime;
                float progress = timer / timeToRotate;

                transform.rotation = Quaternion.Slerp(origin, destination, progress);

                yield return null;
            }

            currentRotationProcess = null;
        }

        private void CharacterModelState_State_OnPropertyChanged(CharacterState state)
        {

        }
    }
}