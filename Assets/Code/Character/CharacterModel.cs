using System;
using UniRx;

namespace ArcheroPrototype
{
    public enum CharacterState { Idle, Move, Rotating }

    public class CharacterModel
    {
        private ReactiveProperty<float> health;
        private ReactiveProperty<CharacterState> state;

        public ReadOnlyReactiveProperty<float> HealthObservable => health.ToReadOnlyReactiveProperty();
        public ReadOnlyReactiveProperty<CharacterState> StateObservable => state.ToReadOnlyReactiveProperty();

        public float Health
        {
            get => health.Value;
            set => health.Value = value;
        }
        public CharacterState State
        {
            get => state.Value;
            set => state.Value = value;
        }

        public CharacterModel()
        {
            health = new ReactiveProperty<float>();
            state = new ReactiveProperty<CharacterState>(CharacterState.Idle);
        }
    }
}
